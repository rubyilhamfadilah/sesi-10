import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:simple_networking/controller/photo_controller.dart';

import 'view/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => PhotoController(),
      child: MaterialApp(
        title: 'API',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const HomePage(title: 'API APP'),
      ),
    );
  }
}
